# Site Guardian PHP Status module

Site Guardian Watchdog Summary adds summary information to the site Status Report regarding the DB Log/Watchdog.

It provides summary information about the entries in the watchdog that are not included in the standard Status report.

Being able to quickly see the number of Emergency/Alert/Critical and Error entries in the log can give you a quick way to see if anything unexpected or untoward is happening that might need attention.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sgd_watchdog_summary).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sgd_watchdog_summary).

## Table of contents

- Recommended modules
- Installation
- Configuration
- Maintainers

## Recommended modules

Site Guardian Watchdog Summary is a companion module for the Site Guardian (API) module and as such will add to the information returned to a consumer of the Site Guardian (API).

See the Site Guardian [project page](https://www.drupal.org/project/site_guardian).

If used in conjunction with the Site Guardian API it returns additional information when a consumer of the Site Guardian (API) requests the sites information.

At present the module returns a full summary of messages as above but also includes informational and warning messages.

Although part of the Site Guardian ecosystem it provides functionality that is useful irrespective of whether the Site Guardian (API) is installed or not and does not require the Site Guardian (API) module to function.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will add information to the Site Status report as described above.

## Maintainers

- Andy Jones - [arcaic](https://www.drupal.org/u/arcaic)
